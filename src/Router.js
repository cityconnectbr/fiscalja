import { createSwitchNavigator, createStackNavigator, createAppContainer } from 'react-navigation';

import AuthLoadingScreen from './Auth/AuthLoadingScreen'

import SignInScreen from './Auth/SignInScreen'
import ResetPasswordScreen from './Auth/ResetPasswordScreen'
import HomeScreen from './Home/HomeScreen'
import OcorrenciaScreen from './Ocorrencia/OcorrenciaScreen'
import PhotoScreen from './Ocorrencia/PhotoScreen'

const AppStack = createStackNavigator({ Home: HomeScreen, Ocorrencia: OcorrenciaScreen, Photo: PhotoScreen });
const AuthStack = createStackNavigator({ SignIn: SignInScreen, Reset: ResetPasswordScreen });

export default createAppContainer(createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  }
));