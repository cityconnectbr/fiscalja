import React, { Component, Fragment } from 'react'
import {
  View, Image, FlatList, ActivityIndicator,
  TouchableHighlight, TouchableOpacity, Button,
} from 'react-native'

import AsyncStorage from '@react-native-community/async-storage'

import {
  Wrapper, Card, CardImage, CardRow, Text
} from './HomeStyle'

import Icon from 'react-native-vector-icons/SimpleLineIcons'

import server from '../Server'

class HomeScreen extends Component {
    constructor(props)  {
      super(props)
      
      this.state = {
        data: [
          { id: 0, secretaria: 'SMMA', reclamacao: 'Poda e Retirada de Árvores', protocolo: '2019-2820543', data: '28/08/2019', local: 'San Remo' },
          { id: 1, secretaria: 'SMMA', reclamacao: 'Poda e Retirada de Árvores', protocolo: '2019-2820543', data: '28/08/2019', local: 'San Remo' },
          { id: 2, secretaria: 'SMMA', reclamacao: 'Poda e Retirada de Árvores', protocolo: '2019-2820543', data: '28/08/2019', local: 'San Remo' },
          { id: 3, secretaria: 'SMMA', reclamacao: 'Poda e Retirada de Árvores', protocolo: '2019-2820543', data: '28/08/2019', local: 'San Remo' },
          { id: 4, secretaria: 'SMMA', reclamacao: 'Poda e Retirada de Árvores', protocolo: '2019-2820543', data: '28/08/2019', local: 'San Remo' },
        ],
        token: '',
        apiUrl: '',
        ocorrencias: [],
        loading: false,
        page: 1,
      }

      this.retrieves()
    }

    retrieves = async () => {
      this.setState({
        token: await AsyncStorage.getItem('userToken'),
        apiUrl: await AsyncStorage.getItem('apiUrl')
      })

      this.carregarOcorrencias()
    }


    static navigationOptions = {
      title: 'Fiscal Já',
      headerStyle: {
        backgroundColor: '#037F8C',
      },
      headerTitleStyle: {
        color: '#f2f2f2',
      },
      headerLeft: (
        <TouchableOpacity style={{ marginLeft: 10, }}>
          <Icon name='menu' color="#f2f2f2" size={24} />
        </TouchableOpacity>
      ),
    };    

    carregarOcorrencias = async () => {
      if(this.state.loading) return;

      const { page, token } = this.state

      this.setState({ loading: true })
      
      const response = await server.get('/ocorrencia?ordem[created_at]=desc&filters[0][status_id][0]=2&paginateCustom=8&page=' + page, token)
      // Concatena os valores antigos com os novos
      this.setState({
        ocorrencias: [...this.state.ocorrencias, ...response.data.data], 
        page: page + 1,
        loading: false,
      })
    }

    renderFooter = () => {
      // if(!this.state.loading) return null;
      return (
        <View>
          <ActivityIndicator size="large" />
        </View>
      )
    }

    goTo(route = "", params= {}) {
      this.props.navigation.navigate(route , params)
    }
  
    render() {
      const { ocorrencias, apiUrl } = this.state;
      return (
        <Fragment>
          <Button title="Actually, sign me out :)" onPress={this._signOutAsync} />
          <Wrapper>
            <View style={{ height: 15 }} />

            { ocorrencias && (
              <FlatList
                data={ocorrencias}
                keyExtractor={item => item.id.toString()}
                renderItem={({item}) => (
                  <View>
                    <TouchableHighlight onPress={() => this.goTo('Ocorrencia', item)}>
                      <Card>
                        <CardImage>
                          { item.subcategoria.imagem ? (
                            <Image source={{ uri: apiUrl + '/' + item.subcategoria.imagem }} style={{ resizeMode: 'cover', width: "100%", height: "100%" }} />  
                          ) : (
                            <Image source={ require('../../assets/image/image-placeholder.png')} style={{ resizeMode: 'cover', width: "100%", height: "100%" }} />
                          )}
                        </CardImage>

                        <CardRow>
                          <Text style={{ fontFamily: 'IBM Plex Sans' }}>{item.subcategoria.categoria.nome} - {item.subcategoria.nome}</Text>
                          <Text>Protocolo: {item.codigo}</Text>
                          <Text>Data: {item.created_at}</Text>
                          <Text>Local: {item.ponto_referencia}</Text>
                        </CardRow>
                      </Card>
                    </TouchableHighlight>
                    <View style={{ height: 15 }} />
                  </View>
                )}
                onEndReached={this.carregarOcorrencias}
                onEndReachedThreshold={0.1}
                ListFooterComponent={this.renderFooter}
              />
            )}
            
          </Wrapper>
        </Fragment>
      );
    }

    _signOutAsync = async () => {
      await AsyncStorage.clear();
      this.props.navigation.navigate('Auth');
    };
}

export default HomeScreen;