import { Dimensions } from 'react-native'
import styled from 'styled-components/native'

const { width, height } = Dimensions.get('screen')

export const Wrapper = styled.View`
  width: ${ width };
  height: 100%;
  background-color: #eee;
  align-items: center;
`

export const Card = styled.View`
  width: ${ width * (95 / 100) };
  height: ${ height * (10 / 100) };
  background-color: transparent;
  flex-wrap: wrap;
`
export const CardImage = styled.View`
  width: ${ width * (30 / 100) };
  height: ${ height * (10 / 100) };
  background-color: #eee;
  box-shadow: 0 13px 21px rgba(38,50,56, 0.5);
  elevation: 2;
`

export const CardRow = styled.View`
  width: ${ width * (65 / 100) };
  height: ${ height * (10 / 100) };
  flex-wrap: wrap;
  justify-content: center;
  align-items: flex-start;
  padding: 0 5px 0 5px;
`

export const Text = styled.Text`
  color:${props => props.color || "#333"}
  box-shadow: 0 13px 21px rgba(38,50,56, 0.5);
  elevation: 2;
  line-height: 20;
  flex-wrap: wrap;
`