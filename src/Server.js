import axios from 'axios'

import { AsyncStorage } from "@react-native-community/async-storage";

userToken = ''

const instance = axios.create({
    baseURL: 'https://homolog-api.fiscalizaja.com.br/api/v1',
    headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json',
    }
})

instance.interceptors.response.use(function(response){
    return response;
}, error => {
    if (error.response.status === 401) {
        refreshUserToken()
    }  
    return Promise.reject(error);
});

const server = {
    get: async (route, token) => {
        try {
            const response = await instance.get(route, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
            })
            userToken = token;
            return response;
        } catch (error) {
            console.warn('get error', error)
        }
    },
    post: async (route, params, token) => {
        try {
            const response = await instance.post(route, params)
            userToken = route === '/login' ? response.data.token : token;
            return response;
        } catch (error) {
            console.warn(error)
        }
    }
}

async function refreshUserToken () {
    try {
        const response = await instance.get('/login/refresh-token', userToken)
        const token = response.data.token
        await AsyncStorage.setItem('userToken', token);
        this.props.navigation.navigate('App');
    } catch(error) {
        console.warn('error refresh', error)
    }
};

export default server;