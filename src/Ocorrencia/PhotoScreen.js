'use strict';
import React, { Component } from 'react';
import { AppRegistry, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { RNCamera } from 'react-native-camera';

class PhotoScreen extends Component {
  static navigationOptions = {
    title: 'Tirar Foto',
    headerStyle: {
      backgroundColor: '#037F8C',
    },
    headerTitleStyle: {
      color: '#fff',
    },
    headerTintColor: '#f2f2f2'
  };

  constructor (props) {
    super(props)

    this.state = {
      focusedScreen: false,
    }
  }
  componentDidMount() {
    const { navigation } = this.props;
    navigation.addListener('willFocus', () =>
      this.setState({ focusedScreen: true })
    );
    navigation.addListener('willBlur', () =>
      this.setState({ focusedScreen: false })
    );
  }

  takePicture = async() => {
    if (this.camera) {
      const options = { quality: 0.2, base64: true };
      const data = await this.camera.takePictureAsync(options);
    }
  };

  render() {
    const {focusedScreen } = this.state;
    if (focusedScreen){
      return (
        <View style={styles.container}>
          <RNCamera
            ref={camera => { this.camera = camera }}
            style = {styles.preview}
            type={RNCamera.Constants.Type.back}
            autoFocus={RNCamera.Constants.AutoFocus.on}
            flashMode={RNCamera.Constants.FlashMode.off}
            captureAudio={false}
            androidCameraPermissionOptions={{
              title: 'Permissão para usar camera',
              message: 'Nós precisamos da sua permissão para usar sua camera.',
              buttonPositive: 'OK',
              buttonNegative: 'Cancelar',
            }}
          />
          <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
            <TouchableOpacity onPress={this.takePicture} style={styles.capture}>
              <Text style={{ fontSize: 14 }}> SNAP </Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
    else {
      return <Text>No access to camera</Text>;
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
});

export default PhotoScreen