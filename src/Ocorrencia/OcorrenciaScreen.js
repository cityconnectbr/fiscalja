import React, { Component, Fragment } from 'react'
import { View, Dimensions, Image, ScrollView,} from 'react-native'

import AsyncStorage from '@react-native-community/async-storage'

import Icon from 'react-native-vector-icons/SimpleLineIcons'

const { width, height } = Dimensions.get('screen')

import styled from 'styled-components/native'

const Wrapper = styled.View`
  width: ${ width };
  background-color: #f2f2f2;
  align-items: center;
  justify-content: center;
`

const Text = styled.Text`
  color:${props => props.color || "#333"};
  text-align: ${props => props.align || 'justify'};
  font-weight: ${props => props.weight || 400};
  font-size: ${props => props.size || '18px'};
  margin-left: ${props => props.mleft || 0};
  box-shadow: 0 13px 21px rgba(38,50,56, 0.5);
  elevation: 5;
  line-height: 20;
`

const CardImage = styled.View`
  width: ${ width };
  height: ${ height * (30 / 100) };
  box-shadow: 0 13px 21px rgba(38,50,56, 0.5);
  elevation: 5;
`

const Content = styled.View`
  width: ${ width };
  height: auto;
  padding: 0 15px 0 15px;
`

const Button = styled.TouchableOpacity`
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 10px;
  width: 120px;
  height: 120px;
  border-radius: 60px;
  background-color: ${props => props.background || '#fff'};
  box-shadow: 0 13px 21px rgba(38,50,56, 0.5);
  elevation: 5;
  opacity: 0.9;
`

const Toolbar = styled.View`
  width: ${ width };
  height: auto;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  position: absolute;
  left: 28%; top: 31%;
`
const ListField = styled.View`
  width: ${ width - 50 };
  height: auto;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  margin-bottom: 10px;
`


class OcorrenciaScreen extends Component {

    static navigationOptions = {
      title: 'Detalhes',
      headerStyle: {
        backgroundColor: '#037F8C',
      },
      headerTitleStyle: {
        color: '#fff',
      },
      headerTintColor: '#f2f2f2'
    };

    componentDidMount() {
      this.retrieves()
    }

    state = {
      ocorrencia: {},
      fotos: [],
      apiUrl: '',
    }


    retrieves = async () => {
      this.setState({
        apiUrl: await AsyncStorage.getItem('apiUrl')
      })
    }

    validarOcorrencia = async () => {
      // Requisição para troca de estado de atenidmento
      // para essa ocorrência aqui.
      console.warn('Ocorrencia Validada')
    }

    goTo(route = "") {
      this.props.navigation.navigate(route)
    }
    
    render() {
      const detalhe = this.props.navigation.state.params
      const { apiUrl } = this.state;

      return (
        <Fragment>
          <Wrapper>
            {/* <View style={{ height: 15 }} /> */}
            <ScrollView>
              <CardImage>
                { detalhe.subcategoria.imagem ? (
                  <Image source={{ uri: apiUrl + '/' + detalhe.subcategoria.imagem }} style={{ resizeMode: 'cover', width: "100%", height: "100%" }} />  
                ) : (
                  <Image source={ require('../../assets/image/image-placeholder.png')} style={{ resizeMode: 'cover', width: "100%", height: "100%" }} />
                )}
              </CardImage>
              
              <View style={{ height: 5 }} />
              
              <Toolbar>
                <Button onPress={() => this.validarOcorrencia()} background="#f2f2f2">
                  <Text weight={600} align="center" color="#4CAF50">Validar{'\n'}Ocorrência</Text>
                  <Icon name="check" size={34} color="#333" color="#4CAF50" />
                </Button>

                <View style={{ height: 5 }} />

                <Button onPress={() => this.goTo('Photo')} background="#f2f2f2">
                  <Text weight={600} align="center" color="#3E2723">Tirar{'\n'}Foto</Text>                 
                  <Icon name="camera" size={34} color="#333" color="#3E2723" />
                </Button>
              </Toolbar>

              <View style={{ height: 15 }} />

              <Content>
                <ListField>
                  <Text color="#263238" weight={600} size="16px">Protocolo</Text>
                  <Text>{ detalhe.codigo }</Text>
                </ListField>

                <ListField>
                  <Text color="#263238" weight={600} size="16px">Secretaria</Text>
                  <Text style={{ fontFamily: 'Foundation', }}>{ detalhe.subcategoria.nome }</Text>
                </ListField>
                
                <ListField>
                  <Text color="#263238" weight={600} size="16px">Data</Text>
                  <Text>{ detalhe.created_at }</Text>
                </ListField>
                  
                <ListField>
                  <Text color="#263238" weight={600} size="16px">Endereço</Text>
                  <Text>{ detalhe.bairro.nome }</Text>
                </ListField>

                <ListField>
                  <Text color="#263238" weight={600} size="16px">Referencia</Text>
                  <Text>{ detalhe.ponto_referencia }</Text>
                </ListField>
                
                <ListField>
                  <Text color="#263238" weight={600} size="16px">Descricao</Text>
                  <Text>{ detalhe.texto }</Text>
                </ListField>
              </Content>
            </ScrollView>
          </Wrapper>
        </Fragment>
      );
    }
}

export default OcorrenciaScreen;