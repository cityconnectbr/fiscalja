/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

import AsyncStorage from '@react-native-community/async-storage'
AsyncStorage.setItem('apiUrl', 'https://homolog-api.fiscalizaja.com.br/api/v1')

AppRegistry.registerComponent(appName, () => App);
