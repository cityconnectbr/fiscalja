/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Fragment} from 'react';
import {
  StatusBar,
} from 'react-native';

import Router from './src/Router';

const App = () => {
  return (
    <Fragment>
      <StatusBar barStyle="dark-content" />
      <Router />
    </Fragment>
  );
};

export default App;
